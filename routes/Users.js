var express = require('express');
var app =express();
const cors = require('cors');

//Get connection
var connection = require('../config');

const users = express.Router();
users.use(cors());

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

//route to handle login and registration
users.post('/api/register',(req,res)=>{
    var today = new Date();
    //var encryptedString = cryptr.encrypt(req.body.password);
    var users = {
        "name" : req.body.name,
        "email" : req.body.email,
        "password" : req.body.password,
        "created_at" : today,
        "updated_at" : today
    }

     const saltRounds = 10;
     //let dbHash;
    //let myPlaintextPassword = req.body.password;

    // myPlaintextPassword = bcrypt.hashSync(pwd,10);
    
    //Check that the user is already a user
    var sql1 = `SELECT name FROM users WHERE email = ?`;
    connection.query(sql1,[req.body.email],(error, rows, fields)=>{
        if(rows.length==0){
            bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                
                users.password  = hash 
                var sql = `INSERT INTO users (name,email,password,created_at,updated_at) VALUES (?, ?, ?, ?, ?)`;
                connection.query(sql,[[req.body.name],[users.email],[users.password],[users.created_at],[users.updated_at]], (error, results, fields)=>{
                    if(error){
                        res.json({
                            status:false,
                            message:'Error with query'
                        })
                    }else{
                        res.json({
                            status:true,
                            data:results,
                            message:'user registered successfully'
                        })
                    }
                });         
            });
            //console.log(rows.length);
        }else{
            res.send('Already a user');
        }
    });
    
    

       
        // console.log(dbHash);
    
});

//user login 
users.post('/api/login',(req, res)=>{
    var email = req.body.email;
    var password = req.body.password;

    var sql = `SELECT * from users WHERE email = ? `;
    connection.query(sql,[email],(err, rows, fields)=>{
        if(rows.length==0){
            res.send('User not Registered');
        }else{
            bcrypt.compare(req.body.password, rows[0].password, (err, result)=>{
                if(err){
                    return res.status(401).json({
                        message: 'Auth failed'
                    });
                }
                if(result){
                    const token = jwt.sign(
                        {
                            email: rows[0].email,  //values that added to the jsonwebtoken 
                            userId: rows[0].id
                        },  process.env.JWT_KEY, {
                            expiresIn: "1h"
                        }
                    );
                    return res.status(200).json({
                        message: 'Auth successful',
                        token: token
                    });
                }
                res.status(401).json({
                    message : 'Auth failed'
                });
            });
            //res.send(rows[0].password);
        }
    });
});


//all users listing
users.get('/api/users', (req, res)=>{
    var sql = 'SELECT * FROM users';
    connection.query(sql, (err, rows, fields)=>{
        if(!err)
            res.json(rows)
        else
            console.log(err);
    });
});

//Get only one specific user
users.get('/api/users/:id',(req, res)=>{
    var sql = 'SELECT * FROM users WHERE id = ?';
    connection.query(sql,[req.params.id],(err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
            console.log(err);
    });
});

//Delete a specific user
users.delete('/api/users/:id',(req, res)=>{
    var sql = 'DELETE FROM users WHERE id = ?';
    connection.query(sql,[req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
            console.log(err);
    });
});

//update a specific user
users.put('/api/users/:id',(req,res)=>{
    var today = new Date();
    var users = {
        "name" : req.body.name,
        "email" : req.body.email,
        "password" : req.body.password,
        "created_at" : today,
        "updated_at" : today
    } 

    var sql = 'UPDATE users SET name = ?, email = ?, updated_at = ?  WHERE id = ? ';
    connection.query(sql,[users.name, users.email , users.updated_at ,req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
            console.log(err);
    });
});



module.exports = users;


