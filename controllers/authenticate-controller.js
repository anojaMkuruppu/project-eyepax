var Cryptr = require('cryptr');
var cryptr = new Cryptr('myTotalSecretKey');

var connection = require('../config');
module.exports.authenticate = (req, res)=>{
    var email = req.body.email;
    var password = req.body.password;

    connection.query('SELECT * FROM users WHERE email = ?',[email], (error, results, fields)=>{
        if(error){
            res.json({
                status:false,
                message:'Error with query'
            })
        }else{
            if(results.length > 0){
                decryptedString = cryptr.decrypt(results[0].password);
                if(password==decryptedString){
                    res.json({
                        status:true,
                        message:'Successfully authenticated'
                    })
                }else{
                    res.json({
                        status:false,
                        message:'Email and password do not match'
                    });
                }
            }else{
                res.json({
                    status:false,
                    message:"Email does not exists"
                });
            }
        }
    });
}