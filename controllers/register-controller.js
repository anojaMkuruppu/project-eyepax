var Cryptr = require('cryptr');
var cryptr = new Cryptr('myTotalySecretKey');
var express = require('express');
var connection = require('../config');

module.exports.register = (req,res)=>{
    var today = new Date();
    var encryptedString = cryptr.encrypt(req.body.password);
    var users = {
        "name" : req.body.name,
        "email" : req.body.email,
        "password" : encryptedString,
        "created_at" : today,
        "updated_at" : today
    }

    connection.query('INSERT INTO users SET ?',users, (error, results, fields)=>{
        if(error){
            res.json({
                status:false,
                message:'Error with query'
            })
        }else{
            res.json({
                status:false,
                data:results,
                message:'user registered successfully'
            })
        }
    });
}